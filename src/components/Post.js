import React,{Component} from 'react';
import Moment from 'react-moment';
import 'moment-timezone';
import {Navbar,Nav,Form,Button,FormControl} from 'react-bootstrap';
import {Link,Redirect,params} from 'react-router-dom';

export default class Post extends Component{
    render(){
        console.log(this.props);
        return (
            <div className="container">
                <h>This is content from post : {this.props.match.params.postId} </h>
            </div>
        );
    }
}