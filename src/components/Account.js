import React,{Component} from 'react';
import { BrowserRouter, Route, Link,Switch} from "react-router-dom";
import queryString from 'query-string';



const animations=[
    {
        name:'Netflix',
        id:'netflix'
    },
    {
        name:'ZillowGroup',
        id:'zillowGroup'
    },
    {
        name:'Yahoo',
        id:'yahoo'
    },
    {
        name:'Modus Create',
        id:'moduscreate'
    },
]

const Show = ({ match, location }) => {
    const value=queryString.parse(location.search)
    return (
      <h2>The <span>name</span> in query String  "{value.name}" </h2>
    );
  }
export default class Account extends Component{
    render(){
        return(
            <div className="container">
            <BrowserRouter>
             <ul>
                 {animations.map(({name,id})=>(
                    <li key={id}>
                        <switch>
                        <Link to={`/account/?name=${id}`}>{name}</Link>
                        </switch>
                    </li>
                   
                 ))}
             </ul>
            
             <Route exact path={`/account/`} component={Show}></Route>
          
           
             </BrowserRouter>
             </div>
        );
    }
}