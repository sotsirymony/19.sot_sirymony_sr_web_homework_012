import React,{Component} from 'react';
import {Card,Button} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Link,Redirect} from 'react-router-dom';
import Moment from 'react-moment';
import 'moment-timezone';



export default class Home extends Component{
   constructor(props)
   {
       super(props);
       this.state=
       {
           redirect:false,
           update:"hello",
    
           users:[
               {id:1},
           ]
           
       };
 
   }
  handleOnclick=()=>{
      this.setState({redirect:true});
  }
    render(){

       var myid;
        if(this.state.redirect)
        {
            myid=this.state.users.map(item=>{
            return <Redirect push to={"/post/"+item.id}></Redirect>
            })
        }
        var d = new Date();
        var n = d.getMinutes()
        
        const ccard=(
            <div className="container">
            <div className="row" id="show-card">
                <div className="col-md-4">
                    <Card style={{ width: '18rem' }}>
                    <Card.Img variant="top" src={ require("../images/ben1.jpg")} />
                     <Card.Body>
                     <Card.Title>Card Title</Card.Title>
                     <Card.Text>
                     Some quick example text to build on the card title and make up the bulk of
                    the card's content.
                     </Card.Text>
                     <Button variant="primary" onClick={this.handleOnclick}>See more</Button>
      
                    </Card.Body>
                    <Card.Footer>
                         <Card.Text>Last Update {n} minute ago </Card.Text>
                    </Card.Footer>
                    </Card>
                </div>
                <div className="col-md-4">
                <Card style={{ width: '18rem' }}>
                    <Card.Img variant="top" src={ require("../images/ben1.jpg")} />
                     <Card.Body>
                     <Card.Title>Card Title</Card.Title>
                     <Card.Text>
                     Some quick example text to build on the card title and make up the bulk of
                    the card's content.
                     </Card.Text>
                    <Button variant="primary"onClick={this.handleOnclick}>See more</Button>
                    </Card.Body>
                    <Card.Footer>
                         <Card.Text>
                            Last Update {n} minute ago
                         </Card.Text>
                    </Card.Footer>
                    </Card>
                </div>
                <div className="col-md-4">
                <Card style={{ width: '18rem' }}>
                    <Card.Img variant="top" src={ require("../images/ben1.jpg")} />
                     <Card.Body>
                     <Card.Title>Card Title</Card.Title>
                     <Card.Text>
                     Some quick example text to build on the card title and make up the bulk of
                    the card's content.
                     </Card.Text>
                    <Button variant="primary"onClick={this.handleOnclick}>See more</Button>
                    </Card.Body>
                    <Card.Footer>
                         <Card.Text>
                            Last Update {n} minute ago
                         </Card.Text>
                    </Card.Footer>
                    </Card>
                </div>
            </div>
            <div className="row" >
                <div className="col-md-4">
                    <Card style={{ width: '18rem' }}>
                    <Card.Img variant="top" src={ require("../images/ben1.jpg")}/>
                     <Card.Body>
                     <Card.Title>Card Title</Card.Title>
                     <Card.Text>
                     Some quick example text to build on the card title and make up the bulk of
                    the card's content.
                     </Card.Text>
                    <Button variant="primary"onClick={this.handleOnclick}>See more</Button>
                    </Card.Body>
                    <Card.Footer>
                         <Card.Text>
                            Last Update {n} minute ago
                         </Card.Text>
                    </Card.Footer>
                    </Card>
                </div>
                <div className="col-md-4">
                <Card style={{ width: '18rem' }}>
                    <Card.Img variant="top" src={ require("../images/ben1.jpg")} />
                     <Card.Body>
                     <Card.Title>Card Title</Card.Title>
                     <Card.Text>
                     Some quick example text
                      to build on the card title and make up the bulk of
                    the card's content.
                     </Card.Text>
                    <Button variant="primary"onClick={this.handleOnclick}>See more</Button>
                    </Card.Body>
                    <Card.Footer>
                         <Card.Text>
                            Last Update {n} minute ago
                         </Card.Text>
                    </Card.Footer>
                    </Card>
                </div>
                <div className="col-md-4">
                <Card style={{ width: '18rem' }}>
                    <Card.Img variant="top" src={ require("../images/ben1.jpg")} />
                     <Card.Body>
                     <Card.Title>Card Title</Card.Title>
                     <Card.Text>
                     Some quick example text to build on the card title and make up the bulk of
                    the card's content.
                     </Card.Text>
                    <Button variant="primary"onClick={this.handleOnclick}>See more</Button>
                    </Card.Body>
                    <Card.Footer>
                         <Card.Text>
                            Last Update {n} minute ago
                         </Card.Text>
                    </Card.Footer>
                    </Card>
                </div>
            </div>
            
            
        </div>
        );
        return(
        <div>
            {ccard},
            {myid}
             
        </div>
        
        );
    }
}