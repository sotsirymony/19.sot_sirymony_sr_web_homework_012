import React,{Component} from 'react';
import { BrowserRouter, Route, Link,Switch} from "react-router-dom";


///////////// declaration animation array ////////////////////////////////////
const animations=[
    {
        name:'Action',
        id:'action'
    },
    {
        name:'Romance',
        id:'romance'
    },
    {
        name:'Comedy',
        id:'comedy'
    }
]
///////////////  declaration movie array /////////////////////////////////////
const  movies=[
    {
        name:'Adventure',
        id:'adventure'
    },
    {
        name:'Comedy',
        id:'comedy'
    },
    {
        name:'Crime',
        id:'crime'
    },
    {
        name:'Documentary',
        id:'documentary'
    },
]

///////////////////////////////////   animation  /////////////////////////////////////
function Animat({match})
{
    const show = animations.find(({id})=>id=== match.params.movieid)
   
    return(<div>
        <h2>{show.name}</h2>
    </div>
    
    )
     
}
function Animation(){
    return (
        <div >
             <h1>
                Animation Category
             </h1>
             <BrowserRouter>
             <ul>
                 {animations.map(({name,id})=>(
                    <li key={id}>
                        <switch>
                        <Link to={`/animation/${id}`}>{name}</Link>
                        </switch>
                    </li>
                 ))}
             </ul>
             <Route exact path={`/animation/:movieid`} component={Animat}></Route>
           
             </BrowserRouter>
             
        </div>
    )
}

////////////////////////////////////  Mvoie  /////////////////////////////////////////
function ShowMovie({match})
{
    const show = movies.find(({id})=>id=== match.params.animationid)
    return(<div>
        <h2>{show.name}</h2>
    </div>
    )
}
function Movie(){
    return (
        <div >
             <h1>
                Movies Category
             </h1>
             <BrowserRouter>
             <ul>
                 {movies.map(({name,id})=>(
                    <li key={id}>
                        <switch>
                        <Link to={`/video/movie/${id}`}>{name}</Link>
                        </switch>
                    </li>
                 ))}
             </ul>
             <Route exact path={`/video/movie/:animationid`} component={ShowMovie}></Route>
            
             </BrowserRouter>
             
        </div>
    )
}


export default class Video extends Component{
    render(){
        const chossen=
        (<div>
           
        </div>)
        return(
            <BrowserRouter>
                <div className="container">
                   
                    <switch>

                    <ul>
                        <li><Link to="/video/animation">Animation</Link></li>
                        <li><Link to="/video/movie">Movie</Link></li>
                    </ul>
                        
                    </switch>
                   
                    <Route exact path='/video/animation' component={Animation}></Route>
                    <Route exact path='/video/movie' component={Movie}></Route>
                    <h2>Please Select A Topic</h2>
                </div>
            </BrowserRouter>
        );
    }
}