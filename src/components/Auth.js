import React,{Component} from 'react';
import {Link,Redirect} from 'react-router-dom';
export default class Auth extends Component{
    state={
        firstname:"",
        password:"",
        redirect:false
    };
    change = e =>{
        this.setState({
            [e.target.firstname]:e.target.value
        }

        )
    }
    handleOnclick=()=>{
        this.setState({redirect:true});
    }
    render(){
        if(this.state.redirect)
        {
            return <Redirect push to={"/welcome"}></Redirect>
        }
       
        return(
            <div className="container">
            <form>
                <input placeholder="First name" value={this.state.firstname} onChange={e=>this.setState({firstname:e.target.value})}></input>
                <input placeholder="Pass Word" value={this.state.password} onChange={e=>this.setState({password:e.target.value})}></input>
                <button onClick={this.handleOnclick}>submit</button>
            </form>
            </div>
        );
    }
}