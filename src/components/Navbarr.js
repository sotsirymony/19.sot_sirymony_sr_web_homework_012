import React,{Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Navbar,Nav,Form,Button,FormControl} from 'react-bootstrap';
import {Link,NavLink } from 'react-router-dom';
import { LinkContainer } from "react-router-bootstrap";
import { NavItem } from "react-bootstrap";

export default class Navbarr extends Component
{
  state={
    isOpen:false
  }
  handleClick=()=>{
    this.setState(
      {
        isOpen:!this.state.isOpen,
      }
    );
  }
    render(){
      console.log(this.props);
        return(
            <div className="container">
            <br />
            <Navbar bg="light" variant="light">
              <Navbar.Brand href="/">ReactRouter</Navbar.Brand>
              <Nav className="mr-auto">
                <Nav.Link as={NavLink} to='/home'>Home</Nav.Link>
                <Nav.Link as={NavLink} to="/video">Video</Nav.Link>
                <Nav.Link as={NavLink} to='/account'>Account</Nav.Link>
                <Nav.Link as={NavLink} to='/auth'>Auth</Nav.Link>
              </Nav>
              <Form inline>
                <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                <Button variant="outline-primary">Search</Button>
              </Form>
            </Navbar>
            {this.props.children}
            </div>
        );
    
        }
}