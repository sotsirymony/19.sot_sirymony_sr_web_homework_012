import React,{Component} from 'react';
import './App.css';
import Default from './components/Default.js';
import Navbarr from './components/Navbarr.js';
import Home from './components/Home.js';
import Auth from './components/Auth.js';
import Video from './components/Video.js';
import { BrowserRouter, Route, Link,Switch} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import Account from './components/Account';
import Post from './components/Post';
import Welcome from './components/Welcome';
import {Fragment} from 'react';
import {render} from 'react-dom'

export default class App extends Component{

render (){
  return(
    <div className="main">
     
        
        <div className="main">
         <Navbarr/>
         <Fragment>
         <Switch>
           <Route exact path="/" component={Default}/>
           <Route exact path="/home" component={Home}/>
           <Route exact path="/video" component={Video}/>
           <Route exact path="/account" component={Account}/>
           <Route exact path="/auth" component={Auth}/>
           <Route exact path="/post/:postId" component={Post}/>
           <Route exact path="/welcome" component={Welcome}/>
          
          
         </Switch>
         </Fragment>
         </div>
    
 
      
      
    </div>
  
  );
  
}
}